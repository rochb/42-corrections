# 42 C Corrections

Hi!
There are some cool C script, they allow you to correct really quickly C00 & C01 projects.
The repo can also be found at www.roch-blondiaux.com/42

# Getting started
You just have to clone the repo, move the proper correction file to the project directory.
Once it's done, compile it with gcc and execute it!
I hope it could help you!
