#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "ex00/ft_strcpy.c"
#include "ex01/ft_strncpy.c"
#include "ex02/ft_str_is_alpha.c"
#include "ex03/ft_str_is_numeric.c"
#include "ex04/ft_str_is_lowercase.c"
#include "ex05/ft_str_is_uppercase.c"
#include "ex06/ft_str_is_printable.c"
#include "ex07/ft_strupcase.c"
#include "ex08/ft_strlowcase.c"
#include "ex09/ft_strcapitalize.c"
#include "ex10/ft_strlcpy.c"
#include "ex11/ft_putstr_non_printable.c"

void printEx(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int	main()
{
	// HEADER
	printf("\033[0;37m----------[\033[0;34mC04\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("mmwww.roch-blondiaux.com\033[0;37m\n\n");

	// Exercice 0
	char zeroSource[] = "azertyuiop"; 
	char zeroDest[10];
	char *zeroResult;
	
	printEx(0, strcmp(zeroSource, ft_strcpy(zeroSource, zeroDest)) == 0);

	// Exercice 1
	char oneSource[] = "azertyuiop"; 
	char oneDest[10];
	char *oneResult;
	
	printEx(1, strcmp(oneSource, ft_strncpy(oneSource, oneDest, 2)) == 0);

	// Exercice 2
	char twoArray[] = "azerty";
	char twoArrayA[] = "azer&tyu5ioP9"; 
	char twoArrayB[] = "";

	printEx(2, ft_str_is_alpha(twoArray) == 1 && ft_str_is_alpha(twoArrayA) == 0 && ft_str_is_alpha(twoArrayB) == 1);

	// Exercice 3
	char threeArray[] = "121212";
	char threeArrayA[] = "azert1yuio"; 
	char threeArrayB[] = "";

	printEx(3, ft_str_is_numeric(threeArray) == 1 && ft_str_is_numeric(threeArrayA) == 0 && ft_str_is_numeric(threeArrayB) == 1);
	
	// Exercice 4
	char fourArray[] = "azertyuiop";
	char fourArrayA[] = "aertYuioP"; 
	char fourArrayB[] = "";

	printEx(4, ft_str_is_lowercase(fourArray) == 1 && ft_str_is_lowercase(fourArrayA) == 0 && ft_str_is_lowercase(fourArrayB) == 1);

	// Exercice 5
	char fiveArray[] = "AZERTYUIOP";
	char fiveArrayA[] = "aertYuiop"; 
	char fiveArrayB[] = "";

	printEx(5, ft_str_is_uppercase(fiveArray) == 1 && ft_str_is_uppercase(fiveArrayA) == 0 && ft_str_is_uppercase(fiveArrayB) == 1);

	// Exercice 6
	char sixArray[] = "AZERTYUIOP";
	char sixArrayA[] = "\n\t"; 
	char sixArrayB[] = "";

	printEx(6, ft_str_is_printable(sixArray) == 1 && ft_str_is_printable(sixArrayA) == 0 && ft_str_is_printable(sixArrayB) == 1);

	// Exercice 7
	char sevenArray[] = "azertyuiop12";
	char sevenArrayA[] = "AZERTYUIOP12"; 

	printEx(7, strcmp(ft_strupcase(sevenArray), sevenArrayA) == 0);

	// Exercice 8
	char eightArray[] = "AZERTYUIOP12";
	char eightArrayA[] = "azertyuiop12"; 

	printEx(8, strcmp(ft_strlowcase(eightArray), eightArrayA) == 0);
	
	// Exercice 9
	char nineArray[] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
	char nineArrayA[] = "Salut, Comment Tu Vas ? 42mots Quarante-Deux; Cinquante+Et+Un"; 
	
	printEx(9, strcmp(ft_strcapitalize(nineArray), nineArrayA) == 0);

	// Exercice 10
	char tenArray[] = "Hello World!";
	char tenArrayA[7];
	unsigned int tenSize = 7;

	printEx(10, ft_strlcpy(tenArrayA, tenArray, tenSize) == strlcpy(tenArrayA, tenArray, tenSize));

	// Exercice 11
	char elevenArray[] = "Salut\n\t";
	ft_putstr_non_printable(elevenArray);

	// NORMINETTE
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");
	// FOOTER
	printf("\n\n----------[\033[0;34mC04\033[0;37m]----------\n");
	return (0);
}


