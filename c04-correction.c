#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "ex00/ft_strlen.c"
#include "ex01/ft_putstr.c"
#include "ex02/ft_putnbr.c"
#include "ex03/ft_atoi.c"
#include "ex04/ft_putnbr_base.c"

void printEx(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int main()
{
	// HEADER
	printf("\033[0;37m----------[\033[0;34mC04\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("mmwww.roch-blondiaux.com\033[0;37m\n\n");

	// Exercice 0
	char zeroArray[] = "azertyuiop";

	printEx(0, ((unsigned long) ft_strlen(zeroArray)) == strlen(zeroArray));

	// Exercice 1
	printf("Exercice \033[0;33m#01\033[0;37m: Should return 'Wazzup buddy?'\n");

	char oneArray[] = "Wazzup buddy?";
	ft_putstr(oneArray);
	printf("\n");

	// Exercice 2
	printf("Exercice \033[0;33m#02\033[0;37m: Should return '-42:42'\033[0;33m\n");
	ft_putnbr(-42);
	write(1, ":", 1);
	ft_putnbr(42);
	printf("\n\n\033[0;37m");

	// Exercice 3
	char threeArray[] = "  ---+--+1234ab567";
	char threeArrayA[] = "    ---+-+--+12345ab567";
	printEx(3, ft_atoi(threeArray) == -1234 && ft_atoi(threeArrayA) == 12345);

	// Exercice 4
	char base[] = "0123456789";
	printf("Exercice \033[0;33m#04\033[0;37m: Should return '-42:42'\033[0;33m\n");
	ft_putnbr_base(-42, base);
	write(1, ":", 1);
	ft_putnbr_base(42, base);
	printf("\n\n\033[0;37m");

	// Exercice 5

	// NORMINETTE
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	// FOOTER
	printf("\n\n----------[\033[0;34mC04\033[0;37m]----------\n");
	return (0);
}
